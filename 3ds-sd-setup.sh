#!/usr/bin/env bash
universalotherapp_version='v1.4.0'
luma3ds_version='v13.0.2'
openfirminstaller_version="v0.0.9"
fastboot3ds_version="v1.2"
godmode9_version='v2.1.1'
twilightmenu_version='v27.0.0'
fbi_version='2.6.1'

BYELLOW='\033[33;1m'
YELLOW='\033[33m'
RED='\033[31m'
NC='\033[0m'

[ -d "$1" ] || {
	echo "Usage: $0 <path to mounted SD card>"
	exit 1
}

[ -n "$XDG_CACHE_HOME" ] \
	&& cachedir="$XDG_CACHE_HOME/3ds-sd/" \
	|| cachedir="$HOME/.cache/3ds-sd/"
mkdir -p "$cachedir"

download() {
	local software="$1"
	local version="$2"
	local url="$3"

	local ext="${url/*./}"

	if [ -f "$cachedir/$software-$version.$ext" ]; then
		printf '%b%s %s already downloaded.%b\n' "$YELLOW" "$software" "$version" "$NC"
	else
		printf '%bDownloading %s %s...%b\n' "$YELLOW" "$software" "$version" "$NC"
		curl -L "$url" -o "$cachedir/$software-$version.$ext" || {
			printf '%bCouldn'"'"'t download %s!%b\n' "$RED" "$software" "$NC"
			return 1
		}
	fi
}

dl_and_extract() {
	local software="$1"
	local version="$2"
	local url="$3"
	local ext="${url/*./}"

	download "$software" "$version" "$url" || return 1

	# Extract
	if [ -d "$cachedir/$software-$version/" ]; then
		printf '%b%s %s already extracted.%b\n' "$YELLOW" "$software" "$version" "$NC"
	else
		printf '%bExtracting %s %s...%b\n' "$YELLOW" "$software" "$version" "$NC"
		if [ "$ext" = 'zip' ]; then
			unzip "$cachedir/$software-$version.zip" -d "$cachedir/$software-$version/" || {
				printf '%bCouldn'"'"'t unzip %s!%b\n' "$RED" "$software" "$NC"
				return 1
			}
		elif [ "$ext" = '7z' ]; then
			7z x "$cachedir/$software-$version.7z" -o"$cachedir/$software-$version/" || {
				printf '%bCouldn'"'"'t extract %s!%b\n' "$RED" "$software" "$NC"
				return 1
			}
		else
			printf '%bInvalid archive!%b\n' "$RED" "$NC"
			return 1
		fi
	fi
}

download universal-otherapp "$universalotherapp_version" "https://github.com/TuxSH/universal-otherapp/releases/download/$universalotherapp_version/otherapp.bin" && {
	printf '%bInstalling universal-otherapp...%b\n' "$BYELLOW" "$NC"
	rsync -c "$cachedir/universal-otherapp-$universalotherapp_version.bin" "$1/arm11code.bin"
}

dl_and_extract Luma3DS "$luma3ds_version" "https://github.com/LumaTeam/Luma3DS/releases/download/$luma3ds_version/Luma3DS$luma3ds_version.zip" && {
	printf '%bInstalling Luma3DS...%b\n' "$BYELLOW" "$NC"
	rsync -cr "$cachedir/Luma3DS-$luma3ds_version/" "$1/"
	mkdir -p "$1/luma/payloads/"
}

dl_and_extract OpenFirmInstaller "$openfirminstaller_version" "https://github.com/d0k3/OpenFirmInstaller/releases/download/$openfirminstaller_version/OpenFirmInstaller-20190405-183939.zip" && {
	printf '%bInstalling OpenFirmInstaller...%b\n' "$BYELLOW" "$NC"
	rsync -c "$cachedir/OpenFirmInstaller-$openfirminstaller_version/OpenFirmInstaller.firm" "$1/luma/payloads/OpenFirmInstaller.firm"
}

dl_and_extract fastboot3DS "$fastboot3ds_version" "https://github.com/derrekr/fastboot3DS/releases/download/$fastboot3ds_version/fastboot3DS$fastboot3ds_version.7z" && {
	printf '%bInstalling fastboot3DS...%b\n' "$BYELLOW" "$NC"
	mkdir -p "$1/ofi/"
	rsync -c "$cachedir/fastboot3DS-$fastboot3ds_version/fastboot3DS.firm" "$1/ofi/"
}

dl_and_extract GodMode9 "$godmode9_version" "https://github.com/d0k3/GodMode9/releases/download/$godmode9_version/GodMode9-$godmode9_version-20220322194259.zip" && {
	printf '%bInstalling GodMode9...%b\n' "$BYELLOW" "$NC"
	mkdir -p "$1/gm9/payloads/"
	rsync -c "$cachedir/GodMode9-$godmode9_version/GodMode9.firm" "$1/gm9/payloads/"
	rsync -cr "$cachedir/GodMode9-$godmode9_version/gm9/scripts/" "$1/gm9/scripts/"
}

dl_and_extract TWiLightMenu "$twilightmenu_version" "https://github.com/DS-Homebrew/TWiLightMenu/releases/download/$twilightmenu_version/TWiLightMenu-3DS.7z" && {
	printf '%bInstalling TWiLightMenu...%b\n' "$BYELLOW" "$NC"
	rsync -cr "$cachedir/TWiLightMenu-$twilightmenu_version/_nds/" "$1/_nds/"
	rsync -c "$cachedir/TWiLightMenu-$twilightmenu_version/BOOT.NDS" "$1/"
	mkdir -p "$1/roms/nds/saves/"
	rsync -c "$cachedir/TWiLightMenu-$twilightmenu_version/TWiLight Menu.cia" "$1/"
}

download FBI "$fbi_version" "https://github.com/lifehackerhansol/FBI/releases/download/$fbi_version/FBI.cia" && {
	printf '%bCopying FBI.cia...%b\n' "$BYELLOW" "$NC"
	mkdir -p "$1/cias/"
	rsync -c "$cachedir/FBI-$fbi_version.cia" "$1/cias/"
}
